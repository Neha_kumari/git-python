public class Array{

   static int row=4;
    static int col=4;
     static int initialValue=10;
     static int [][] array = new int[row][col];
    public static void main(String args[]){
       //int [] intArray = new int[]{11,12,13,14,15};
      
      
       populateTwoDimensionArray();
       displayTwoDimensionArray();
}

    public static void populateTwoDimensionArray(){
        for(int rowCount=0; rowCount<4; rowCount++){
            for(int colCount=0; colCount<4; colCount++){
                array[rowCount][colCount] = initialValue++;
            }

        }
    }
     public static void displayTwoDimensionArray(){
        for(int rowCount=0; rowCount<4; rowCount++){
            for(int colCount=0; colCount<4; colCount++){
                System.out.print(array[rowCount][colCount]+" ") ;
            }
            System.out.println();

        }
     }
}  