public class SalaryAcc extends BankApp{
   
    public SalaryAcc(String name, int AccNo) {
		super(name, AccNo);
		
	}

	public static double minBal=0;

    @Override
    public void withdrawAmount(double amount){
        if(minBal==0 && minBal<amount){
            System.out.println("Can not withdraw money ");
        }
        else{
            minBal= minBal-amount;
            System.out.println("The available balance is" +minBal);
        }
      }
    
      @Override
      public void depositAmount(double amount){
              minBal= minBal+amount;
              System.out.println("The available balance is" +minBal);
      }
    
}